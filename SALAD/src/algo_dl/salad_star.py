#!/usr/bin/env python
# coding: utf-8

import copy
from itertools import islice

import numpy as np
import torch

from algo_dl.salad_base import BaseSalad


class SaladStar(BaseSalad):

    def __init__(self, testloader, trainloader_init, net, param_optimizer, load_init = True, save_init = True,
                 path_server = None):
        super().__init__(testloader, trainloader_init, net, param_optimizer, load_init, save_init, path_server)
        self.net_ref_pt = copy.deepcopy(net)
        # initialize the variance reduction dictionary
        self.grad_ref_pt = dict()
        for i in range(-1, self.num_clients):
            self.grad_ref_pt[i] = {}

    def net_clients_update(self, trainloader, wd, lr):
        running_loss = 0.
        correct = 0
        total = 0
        gaussians = {}
        for name, param in self.net_server.named_parameters():
            gaussians[name] = torch.sqrt(2 / (lr * self.num_clients)) * torch.normal(mean=torch.zeros_like(param),
                                                                                     std=torch.ones_like(param))
        length = len(trainloader)
        index = np.random.randint(length)
        inputs, targets = list(islice(trainloader, index, index + 1))[0]
        # the i-th worker updates it local parameter
        inputs, targets = torch.transpose(inputs, dim0=0, dim1=1).to(self.device), targets.to(self.device)
        for i, (net, batch) in enumerate(zip(self.net_clients, inputs)):
            net.zero_grad()
            outputs = net(batch).to(self.device)
            loss = self.criterion(outputs, targets[:, i])
            for param in net.parameters():
                loss += wd * torch.norm(param) ** 2
            # compute the gradient of loss with respect to all Tensors with requires_grad=True
            loss.backward()
            running_loss += loss.item()
            # the class with the highest energy is what we choose as prediction
            _, predicted = torch.max(outputs.data, 1)
            total += targets[:, i].size(0)
            correct += (predicted == targets[:, i]).sum().item()
            # disable gradient calculation to reduce memory consumption
            with torch.no_grad():
                for name, param in net.named_parameters():
                    # compute the gradient transmitted by the i-th worker
                    scaled_grad = self.num_data * param.grad.data - self.grad_ref_pt[i][name] \
                                  + self.grad_ref_pt[-1][name] + gaussians[name]
                    # perform the SGD step
                    param.data.sub_(lr * scaled_grad)
        print('--- Train --- Accuracy = %.3f, Loss = %.3f' % (100 * correct / total, running_loss))

    @torch.no_grad()
    def ref_pt_update(self, pvals):
        # Parameter updates
        for param_ref_pt in self.net_ref_pt.parameters():
            param_ref_pt.data.copy_(torch.zeros_like(param_ref_pt).to(self.device))
        for p, net in zip(pvals, self.net_clients):
            for param_ref_pt, param_client in zip(self.net_ref_pt.parameters(), net.parameters()):
                param_ref_pt.data.add_(p * param_client)

    def cv_update(self, trainloader, wd):
        with torch.no_grad():
            for name, param_ref_pt in self.net_ref_pt.named_parameters():
                for i in range(self.num_clients):
                    self.grad_ref_pt[i][name] = 2 * self.num_data * wd * param_ref_pt
        for inputs, targets in trainloader:
            inputs, targets = torch.transpose(inputs, dim0=0, dim1=1).to(self.device), targets.to(self.device)
            batch_size = len(targets[:, 0])
            for i, batch in enumerate(inputs):
                self.net_ref_pt.zero_grad()
                outputs = self.net_ref_pt(batch).to(self.device)
                loss = self.criterion(outputs, targets[:, i])
                # compute the gradient of loss with respect to all Tensors with requires_grad=True
                loss.backward()
                # disable gradient calculation to reduce memory consumption
                with torch.no_grad():
                    for name, param_ref_pt in self.net_ref_pt.named_parameters():
                        self.grad_ref_pt[i][name] += batch_size * param_ref_pt.grad.data
        with torch.no_grad():
            for name, _ in self.net_ref_pt.named_parameters():
                self.grad_ref_pt[-1][name] = torch.stack(
                    [self.grad_ref_pt[i][name] for i in range(self.num_clients)]).mean(dim=0)

    def run(self, trainloader, testloader, num_iter, weight_decay, prob_communication, lr_dict, pvals, t_burn_in=0,
            thinning=1, epoch_init=-1, save_samples=False, path_samples=None):
        prob_update_param, prob_update_cv = prob_communication.values()
        self.num_data = 0  # the number of data on the clients
        for loader_i in trainloader:
            self.num_data += sum([len(data[1]) for data in loader_i])
        wd = weight_decay / (self.num_clients * self.num_data)
        scheduler = lr_dict['scheduler'](**lr_dict['args'])
        self.ref_pt_update(pvals)
        self.cv_update(trainloader, wd)
        self.net_server_update(pvals)
        for epoch in range(epoch_init + 1, epoch_init + 1 + num_iter):
            scheduler.step()
            lr = torch.Tensor([scheduler.get_lr()]).to(self.device)
            communication_round = np.random.binomial(1, prob_update_param, 1)
            cv_update_round = np.random.binomial(1, prob_update_cv, 1)
            if cv_update_round:
                self.ref_pt_update(pvals)
            self.net_clients_update(trainloader, wd, lr)
            if cv_update_round:
                self.cv_update(trainloader, wd)
            if communication_round == 1:
                print('\n-- COMMUNICATION --')
                self.net_server_update(pvals)
                self.net_server_transfert()
            self.save_results(testloader, epoch, t_burn_in, thinning, save_samples, path_samples, pvals)
        return self.net_server.state_dict(), self.save_dict

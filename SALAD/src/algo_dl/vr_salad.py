#!/usr/bin/env python
# coding: utf-8

import copy
from itertools import islice

import numpy as np
import torch

from algo_dl.salad_base import BaseSalad


class VrSalad(BaseSalad):

    def __init__(self, testloader, trainloader_init, net, param_optimizer, load_init = True, save_init = True,
                 path_server = None):
        super().__init__(testloader, trainloader_init, net, param_optimizer, load_init, save_init, path_server)
        # create the network net_svrg
        self.net_svrg = [copy.deepcopy(net) for _ in range(self.num_clients)]
        [net_svrg.to(self.device) for net_svrg in self.net_svrg]
        net_state_dict = copy.deepcopy(net.state_dict())
        # initialize the variance reduction parameter
        self.grad_svrg = dict()
        for i in range(self.num_clients):
            self.grad_svrg[i] = {}
            for name, param in net_state_dict.items():
                self.grad_svrg[i][name] = torch.zeros_like(param)

    def net_clients_update(self, trainloader, wd, lr, cv_update_round):
        running_loss = 0.
        correct = 0
        total = 0
        gaussians = {}
        for name, param in self.net_server.named_parameters():
            gaussians[name] = torch.sqrt(2 / (lr * self.num_clients)) * torch.normal(mean=torch.zeros_like(param),
                                                                                     std=torch.ones_like(param))
        length = len(trainloader)
        index = np.random.randint(length)
        inputs, targets = list(islice(trainloader, index, index + 1))[0]
        # the i-th worker updates it local parameter
        inputs, targets = torch.transpose(inputs, dim0=0, dim1=1).to(self.device), targets.to(self.device)
        for i, (net, net_svrg, batch) in enumerate(zip(self.net_clients, self.net_svrg, inputs)):
            net_state_dict = copy.deepcopy(net.state_dict()) if cv_update_round else None
            net.zero_grad()
            outputs = net(batch).to(self.device)
            loss = self.criterion(outputs, targets[:, i])
            for param in net.parameters():
                loss += wd * torch.norm(param) ** 2
            # compute the gradient of loss with respect to all Tensors with requires_grad=True
            loss.backward()
            running_loss += loss.item()
            # the class with the highest energy is what we choose as prediction
            _, predicted = torch.max(outputs.data, 1)
            total += targets[:, i].size(0)
            correct += (predicted == targets[:, i]).sum().item()
            # compute the gradient at the reference point
            net_svrg.zero_grad()
            outputs_svrg = net_svrg(batch).to(self.device)
            loss_svrg = self.criterion(outputs_svrg, targets[:, i])
            for param_svrg in net_svrg.parameters():
                loss_svrg += wd * torch.norm(param_svrg) ** 2
            loss_svrg.backward()
            # disable gradient calculation to reduce memory consumption
            with torch.no_grad():
                for (name, param), param_svrg in zip(net.named_parameters(), net_svrg.parameters()):
                    # compute the gradient transmitted by the i-th worker
                    scaled_grad = self.num_data * (param.grad.data - param_svrg.grad.data) + self.grad_svrg[i][name]
                    # perform the SGD step
                    param.data.sub_(lr * (scaled_grad + gaussians[name]))
            # Update the svrg network  # todo: pb il faut mettre jour avec l'ancienne cv.
            if cv_update_round:
                self.cv_update(i, net_svrg, net_state_dict, trainloader, wd)
        print('--- Train --- Accuracy = %.3f, Loss = %.3f' % (100 * correct / total, running_loss))

    def cv_update(self, i, net_svrg, net_state_dict, trainloader, wd):
        net_svrg.load_state_dict(net_state_dict)
        with torch.no_grad():
            for name, param_svrg in net_svrg.named_parameters():
                self.grad_svrg[i][name] = 2 * self.num_data * wd * param_svrg
        for inputs, targets in trainloader:
            inputs, targets = torch.transpose(inputs, dim0=0, dim1=1).to(self.device), targets.to(self.device)
            batch_size = len(targets[:, i])
            net_svrg.zero_grad()
            outputs_svrg = net_svrg(inputs[i]).to(self.device)
            loss_svrg = self.criterion(outputs_svrg, targets[:, i])
            loss_svrg.backward()
            # save the gradient of the i-th worker
            for name, param_svrg in net_svrg.named_parameters():
                self.grad_svrg[i][name] += batch_size * param_svrg.grad.data

    def run(self, trainloader, testloader, num_iter, weight_decay, prob_communication, lr_dict, pvals, t_burn_in = 0,
            thinning = 1, epoch_init = -1, save_samples = False, path_samples = None):
        prob_update_param, prob_update_cv = prob_communication.values()
        self.num_data = 0  # the number of data on the clients
        for loader_i in trainloader:
            self.num_data += sum([len(data[1]) for data in loader_i])
        wd = weight_decay / (self.num_clients * self.num_data)
        for i, net_svrg in enumerate(self.net_svrg):
            net_state_dict = net_svrg.state_dict()
            self.cv_update(i, net_svrg, net_state_dict, trainloader, wd)
        scheduler = lr_dict['scheduler'](**lr_dict['args'])
        self.net_server_update(pvals)
        for epoch in range(epoch_init + 1, epoch_init + 1 + num_iter):
            scheduler.step()
            lr = torch.Tensor([scheduler.get_lr()]).to(self.device)
            cv_update_round = np.random.binomial(1, prob_update_cv, 1)
            self.net_clients_update(trainloader, wd, lr, cv_update_round)
            communication_round = np.random.binomial(1, prob_update_param, 1)
            if communication_round == 1:
                print('\n-- COMMUNICATION --')
                self.net_server_update(pvals)
                self.net_server_transfert()
            self.save_results(testloader, epoch, t_burn_in, thinning, save_samples, path_samples, pvals)
        return self.net_server.state_dict(), self.save_dict

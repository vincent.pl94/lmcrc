# SALAD: Efficient federated Bayesian sampling by Stochastic Averaging Langevin Dynamics.

This repository contains the code to reproduce the experiments in the paper *SALAD: Efficient federated Bayesian sampling by Stochastic Averaging Langevin Dynamics* by Vincent Plassier, Alain Durmus and Eric Moulines.

## Requirements

We use provide a `requirements.txt` file that can be used to create a conda
environment to run the code in this repo:
```bash
$ conda create --name <env> --file requirements.txt
```

Example set-up using `pip`:
```bash
$ pip install --upgrade pip
$ pip install -r requirements.txt
```

## Abstract

In this work, we develop new methods for Bayesian computation in a federated learning context (FL). While there are a variety of distributed MCMC algorithms, few have been developed to address the specific constraints of FL, such as data privacy, communication overhead, statistical heterogeneity. To tackle these issues, we propose *SALaD*, a MCMC algorithm that combines the ideas of Stochastic Langevin Gradient Dynamics and Federated Averaging. In each round, each client executes *SGLD* to update its local parameter, which is sent to a central server. The central server then in turn sends the average of the local parameters to the clients. However, this method may suffer from the high variance of the stochastic gradients used by local *SGLD* and the heterogeneity of the data, which hinders and/or slows down convergence. To address these issues, we propose three alternatives based on a combination of control variates and bias reduction techniques for which theoretical improvements are derived. We illustrate our findings using several FL benchmarks for Bayesian inference.
